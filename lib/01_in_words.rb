class Integer

  def in_words
    number_size(self)
  end

  private

  def up_to_nine(number)
    words = {
      0 => "zero", 1 => "one", 2 => "two", 3 => "three", 4 => "four",
      5 => "five", 6 => "six", 7 => "seven", 8 => "eight", 9 => "nine"
    }
    words[number]
  end

  def ten_to_twelve(number)
    words = { 10 => "ten", 11 => "eleven", 12 => "twelve" }
    words[number]
  end

  def thirteen_to_nineteen(number)
    words = {
      13 => "thirteen", 14 => "fourteen", 15 => "fifteen",
      16 => "sixteen", 17 => "seventeen", 18 => "eighteen",
      19 => "nineteen"
    }
    words[number]
  end

  def tens(number)
    words = {
      2 => "twenty", 3 => "thirty", 4 => "forty",
      5 => "fifty", 6 => "sixty", 7 => "seventy",
      8 => "eighty", 9 => "ninety"
    }
    tens = number / 10
    single = number - (tens * 10)
    word = ""
    word << words[tens]
    word << " " + number_size(single) if single > 0
    word
  end

  def hundreds(number)
    hundreds = number / 100
    tens = number - (hundreds * 100)
    word = ""
    word << up_to_nine(hundreds) + " hundred"
    word << " " + number_size(tens) if tens > 0
    word
  end

  def thousands(number)
    thousands = number / 1000
    hundreds = number - (thousands * 1000)
    word = ""
    word << number_size(thousands) + " thousand"
    word << " " + number_size(hundreds) if hundreds > 0
    word
  end

  def millions(number)
    millions = number / 1000000
    remainder = number - (millions * 1000000)
    word = ""
    word << number_size(millions) + " million"
    word << " " + number_size(remainder) if remainder > 0
    word
  end

  def billions(number)
    billions = number / 1000000000
    remainder = number - (billions * 1000000000)
    word = ""
    word << number_size(billions) + " billion"
    word << " " + number_size(remainder) if remainder > 0
    word
  end

  def trillions(number)
    trillions = number / 1000000000000
    remainder = number - (trillions * 1000000000000)
    word = ""
    word << number_size(trillions) + " trillion"
    word << " " + number_size(remainder) if remainder > 0
    word
  end

  def ten_to_hundred(number)
    if (10..12).cover?(number)
      ten_to_twelve(number)
    elsif (13...20).cover?(number)
      thirteen_to_nineteen(number)
    elsif (20...100).cover?(number)
      tens(number)
    end
  end

  def number_size(number)
    if (0...10).cover?(number)
      up_to_nine(number)
    elsif (10...100).cover?(number)
      ten_to_hundred(number)
    elsif (100...1000).cover?(number)
      hundreds(number)
    elsif (1000...1000000).cover?(number)
      thousands(number)
    elsif (1000000...1000000000).cover?(number)
      millions(number)
    elsif (1000000000...1000000000000).cover?(number)
      billions(number)
    elsif (1000000000000...1000000000000000).cover?(number)
      trillions(number)
    else
      raise "#{number} is too large for this context and
        cannot be put into words"
    end
  end
end
